

let fs = require('fs')
let app = require('../server')
const ZONE = {"N":["Lào Cai","Yên Bái","Điện Biên","Hoà Bình","Lai Châu","Sơn La","Hà Giang","Cao Bằng","Bắc Kạn","Lạng Sơn","Tuyên Quang","Thái Nguyên","Phú Thọ","Bắc Giang","Quảng Ninh","Bắc Ninh","Hà Nam","Hà Nội","Hải Dương","Hải Phòng","Hưng Yên","Nam Định","Ninh Bình","Thái Bình","Vĩnh Phúc"],"M":["Thanh Hoá","Nghệ An","Hà Tĩnh","Quảng Bình","Quảng Trị","Thừa Thiên-Huế","Đà Nẵng","Quảng Nam","Quảng Ngãi","Bình Định","Phú Yên","Khánh Hoà","Ninh Thuận","Bình Thuận","Kon Tum","Gia Lai","Đắc Lắc","Đắc Nông","Lâm Đồng"],"S":["Bình Phước","Bình Dương","Đồng Nai","Tây Ninh","Bà Rịa-Vũng Tàu","Thành phố Hồ Chí Minh","Long An","Đồng Tháp","Tiền Giang","An Giang","Bến Tre","Vĩnh Long","Trà Vinh","Hậu Giang","Kiên Giang","Sóc Trăng","Bạc Liêu","Cà Mau","Thành phố Cần Thơ"]}


module.exports.companyManage = async (req, res) => {
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Image',
      key: 'avatar',
      image: true
    },
    {
      title: 'Tên công ty',
      key: 'name'
    },
    {
      title: 'Email',
      key: 'email'
    },
    {
      title: 'SĐT',
      key: 'phone'
    },
    {
      title: 'Giám đốc',
      key: 'manager'
    },
    {
      title: 'Website',
      key: 'website'
    },
    {
      title: 'Địa chỉ Văn phòng',
      key: 'office_address'
    },
    {
      title: 'Thành phố',
      key: 'city'
    },
  ]

  let limit = parseInt(req.query.limit) || 25
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let companyModel = app.models.company
  let companies = [], totalPage = 0
  let where = {}
  let fieldToQuery = req.query.search_field || 'name'

  if (req.query.q) {
    where = {
      [fieldToQuery]: { like: `%${req.query.q}%` }
    }
  }
  let fieldLabel = ({"name": "Tên công ty", "manager": "Giám đốc", "office_address": "Địa chỉ văn phòng", "email": "Email", "city": "Thành phố", "website": "Website"})[fieldToQuery]
  console.log(fieldLabel, fieldToQuery)
  try {
    companies = await companyModel.find({
      where,
      limit,
      skip,
      order: 'id DESC'
    })
    let companiesCount = await companyModel.count({where})
    totalPage = Math.ceil(companiesCount/limit)
  } catch (err) { res.end(err.message); console.log(err)}
  res.render("company-manager", {companies, headers, totalPage, fieldToQuery, queryString: req.query.q || '', qlabel: (fieldLabel || 'Tên công ty')})
}



module.exports.companyCreate = async (req, res) => {
  res.render("company-edit", {company: {}})
}


module.exports.companyEdit = async (req, res) => {
  let companyId = req.params.companyId
  let companyModel = app.models.company
  let company = {}
  try {
    company = await companyModel.findById(companyId)
  }
  catch (err) {
    res.end(err.message)
  }
  
  if (!company) {
    return res.end('NOT FOUND');
  }
  company.zone_name = ({N: 'Miền Bắc', M: 'Miền Trung', S: 'Miền Nam'})[company.zone]
  res.render("company-edit", {company})
}
