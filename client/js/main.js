function parseQuery(queryString) {
  var query = {};
  var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (var i = 0; i < pairs.length; i++) {
      var pair = pairs[i].split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
  }
  return query;
}

function buildQuery(obj){
  let str = ""
  Object.keys(obj).forEach(k => {
    if (k == "") return
    str += (str === "" ? "?" : "&") + k + "=" + obj[k]
  })
  return str
}

window.vnk = {}

vnk.createPaging = function (totalPage) {
  let query = parseQuery(location.search)
  let page = parseInt(query.page || 1)
  if (!totalPage) {
    // Next back only
    query.page = Math.max(1, page - 1)
    let prevPageLink = location.pathname + buildQuery(query)
    query.page = page+1;
    let nextPageLink = location.pathname + buildQuery(query)
    return `<nav aria-label="pagination">
      <ul class="pagination">
        <li class="page-item"><a class="page-link" href="${prevPageLink}">Previous</a></li>
        <li class="page-item"><a class="page-link" href="${nextPageLink}">Next</a></li>
      </ul>
    </nav>`
  } else{
    let forwardArray = []
    let start = page
    while(start < page+3 && start < totalPage) {
      query.page = start
      forwardArray.push({
        link: location.pathname + buildQuery(query),
        class: start == page ? "active" : "",
        title: start
      })
      start +=1
    }
    let backwardArray = []
    start = page-1
    while(start > page-4 && start > 0) {
      query.page = start
      backwardArray.push({
        link: location.pathname + buildQuery(query),
        class: "",
        title: start
      })
      start -=1
    }
    if (page > 1) {
      query.page = 1
      backwardArray.push({
        title: "Trang đầu",
        link: location.pathname + buildQuery(query),
        class: page == 1 ? "active" : ""
      })
    }
    if (totalPage > 1) {
      query.page = totalPage
      forwardArray.push({
        title: "Trang cuối",
        link: location.pathname + buildQuery(query),
        class: page == totalPage ? "active" : ""
      })
    }
    backwardArray = backwardArray.reverse().concat(forwardArray)
    console.log(backwardArray)
    return '<nav aria-label="pagination">'+
      '<ul class="pagination">'
      + backwardArray.map(e => ('<li class="page-item '+e.class+'"><a class="page-link" href="'+e.link+'">'+ (e.title || e.page) +'</a></li>')).join("")
      +'</ul></nav>'
  }
}


vnk.enableDelete = function (){
  $(".delete-link").each(function (i, e) {
    if (localStorage.getItem('admin') == 2 ) {
      $(e).show()
    }
    e.onclick = function (event) {
      event.preventDefault()
      let id = $(e).data("id"), apiUrl = $(e).data("api")
      if (! (localStorage.getItem('admin') == 2) ){
        alert("Permission denied")
        return
      }
      if (confirm("Cảnh báo: Bạn chắc chắn muốn xoá dữ liệu? Thao tác xoá sẽ không thể khôi phục lại.")) {
        $.ajax({
          type: "DELETE",
          url: apiUrl + "/" + id
        })
        .done(res => {
          location.reload()
        })
        .fail(err => {
          alert(err.message)
        })
      }
    }
  })
}

