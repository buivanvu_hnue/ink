'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
var path = require('path');
var ect = require('ect');

var app = module.exports = loopback();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser')
var multer  = require('multer');

var fileController = require('./controllers/file');
var appHandler = require('./router')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ect');
app.use(cookieParser());


app.use(loopback.token({  
  cookies: ['access_token'],
  headers: ['access_token', 'Authentication'],
  params:  ['access_token', 'Authentication']
}));

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './client/uploads');
  },
  filename: function(req, file, cb) {
    console.log(file);
    cb(null, file.fieldname + '-' + Date.now() + file.originalname);
  },
});

var upload = multer({storage: storage});

const viewDir = path.resolve(process.cwd(), 'views');
app.engine('ect', ect({
  watch: true,
  root: viewDir,
  ext: '.ect',
  open: '{%',
  close: '%}',
}).render);

app.post('/file-upload', upload.single('file'), fileController.upload);

app.use('/api', function (req, res, next) {
  console.log('Time:', Date.now())
  console.log(req.method + ':' + req.url);
  next();
  //////
})

app.use(/^\/(css|js|images|uploads).*$/, function(req, res, next) {
  res.setHeader('Cache-Control', 'private, max-age=240000');
  next();
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  appHandler(app);
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
