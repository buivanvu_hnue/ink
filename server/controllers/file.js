
let fs = require('fs')
let app = require('../server');

module.exports.upload = async (req, res) => {
  let file = req.file
  let model = app.models.file
  let data = {file_name: file.filename, link: 'http://vnk.vn/uploads/' + file.filename, type: file.mimetype, path: file.path, created: new Date()}
  model.create(data, (arg) => {res.json(data)})
}