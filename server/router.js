
let homeCtrl = require('./controllers/home')
let companyCtrl = require('./controllers/company')
let projectCtrl = require('./controllers/project')

module.exports = function (app) {
  app.get('/', homeCtrl.dashboard);
  app.get('/login', homeCtrl.login);
  app.get('/logout', homeCtrl.logout);
  app.get('/project-manager', homeCtrl.projectManage)
  app.get('/project/:projectId/edit', homeCtrl.projectEdit)
  app.get('/project/create', homeCtrl.createProject)
  app.get('/project/import', (req, res) => res.render('project-import'))
  app.get('/user-manager', homeCtrl.userManage)
  app.get('/user/:userId/edit', homeCtrl.userEdit)
  app.get('/user/create', homeCtrl.userCreate)
  app.post('/api/user/:userId/update', homeCtrl.userUpdate)
  app.get('/orders', homeCtrl.orderList)
  app.get('/sale-team', homeCtrl.saleTeamManage)
  app.get('/sale-team/:teamId/edit', homeCtrl.teamEdit)
  app.get('/company/import', (req, res) => res.render('company-import'))
  app.get('/company', companyCtrl.companyManage)
  app.get('/company/:companyId/edit', companyCtrl.companyEdit)
  app.get('/company/create', companyCtrl.companyCreate)
  app.get('/notification', homeCtrl.notifyManager)
  app.post('/send-system-notify', homeCtrl.sendSystemNotification)
  app.post('/project/register-contacts', projectCtrl.registerContacts)
  app.delete('/api/user-delete/:userId', homeCtrl.userDelete)
}