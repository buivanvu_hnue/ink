
let fs = require('fs')
let moment = require('moment')
let app = require('../server')

async function updateProject(offset) {
  const MAX_STATUS_CODE = 8
  const STATUS = {
    "0": "",
    "1": {text: "Sàng lọc", code: "sang_loc"},
    "2": {text: "Gọi điện", code: "goi_dien"},
    "3": {text: "Giới thiệu công ty", code: "gioi_thieu_cong_ty"},
    "4": {text: "Thuyết trình giải pháp", code: "thuyet_trinh_giai_phap"},
    "5": {text: "Chào giá", code: "chao_gia"},
    "6": {text: "Thương thảo giá", code: "thuong_thao_gia"},
    "7": {text: "Thương thảo hợp đồng", code: "thuong_thao_hop_dong"},
    "8": {text: "CLOSE", code: "close"},
  }
  let {FollowedProject, Notification, task} = app.models
  let scheduleModel = app.models.schedule
  let projects = await FollowedProject.find({
    where: {status_code: {lt: MAX_STATUS_CODE}},
    limit: 1500,
    skip: offset || 0
  })
  for (let project of projects) {
    // check if project is out_dated
    // find schedule setting
    let where = {}
    if (project.team_id > 0) {
      where = {team_id: project.team_id}
    } else {
      where = {user_id: project.team_id}
    }
    // try
    try {
    let schedule = await scheduleModel.findOne(where)
    if (!schedule) {
      continue;
    }
    let currentStatus = project.status_code
    let lastUpdated = moment(project.last_status_updated || project.last_modified)
    let code = (STATUS[currentStatus] && STATUS[currentStatus].code) || "sang_loc"
    let dayOffset = schedule[code]
    lastUpdated.add(dayOffset, "days")
    let today = moment().endOf("day")
    if (lastUpdated.toDate() <= today.toDate()) {
      // Next status
      let body = `Dự án ${project.name} đã quá hạn, cần được chuyển sang trạng thái ${project.status}`
      let notification = {
        "from": project.updated_by || 0,
        "data": {
         "body": body,
         "title": "Dự án quá hạn",
         "notification": {
              "body": body,
              "title": "Dự án quá hạn",
              "color": "#00ACD4",
              "priority": "high",
              "id": "project_status_need_update",
              "show_in_foreground": "true",
              "idDetect": project.id
          }
       }
      }
      if (project.team_id > 0){
        let usersToSent = []
        let tasks = await task.find({
          where: {
            project_id: project.id
          }
        })
        for (let _task of tasks) {
          if (usersToSent.indexOf(_task.user_id) < 0) {
            usersToSent.push(_task.user_id)
          }
          if (usersToSent.indexOf(_task.created_by) < 0) {
            usersToSent.push(_task.created_by)
          }
        }
        if (usersToSent.indexOf(project.user_id) <0) {
          usersToSent.push(project.user_id)
        }
        for (let userId of usersToSent) {
          await Notification.sendNotifyTo(userId, notification)
        }
        
      } else {
        console.log('SENT_TO_USER')
        await Notification.sendNotifyTo(project.user_id, notification)
      }
      // await project.save()
    }
    //
    } catch (err) {
      console.log(err)
    }
  }
  return 1
}

var CronJob = require('cron').CronJob;

new CronJob('00 00 22 * * *', function() {
  updateProject(0)
  .then(result => {
    console.log("CRON JOB DONE SUCCESS")
  })
  .catch(err => {
    console.log(err)
  })
}, null, true, 'Asia/Ho_Chi_Minh');

if (process.argv.includes("update")) {
  updateProject(0)
  .then(result => {
    console.log("JOB DONE RIGHT NOW")
  })
  .catch(err => {
    console.log(err)
  })
}

console.log("Enter update argv to force new update operation")
