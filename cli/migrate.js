'use strict';

let path = require('path');
var argv = require('minimist')(process.argv.slice(2));

let app = require(path.resolve(__dirname, '../server/server'));
let ds = app.datasources.mydb;

function migrateModel(model) {
  return new Promise((resolve, reject) => {
    ds.automigrate(model, function(err) {
      if (err) reject(err);
      resolve(1);
    });
  });
}

let modelList = []
if (argv.m) {
  modelList = argv.m.split(',');
} else {
  let ModelsConfig = require('../server/model-config.json')
  delete ModelsConfig._meta
  modelList = Object.keys(ModelsConfig)
}
async function mainProc() {
  for (let model of modelList) {
    await migrateModel(model)
    console.log('MIGRATED: ', model)
  }
}

mainProc().then(r => { console.log('DONE!'); process.exit(0) }).catch(err=>{ console.log(err); process.exit(err.code || 1) })
