'use strict';
let app = require('../../server/server')
let moment = require('moment')

module.exports = function (Task) {


  Task.observe('after save', async function(ctx) {
    let {user, Notification} = app.models
    if (ctx.instance && ctx.isNewInstance) {
      await Notification.sendNotifyTo(ctx.instance.user_id, {
        "from": ctx.instance.created_by || '0',
        "data": {
         "body": ctx.instance.content,
         "title": "Nhận được công việc",
         "notification": {
              "body": ctx.instance.content,
              "title": "Nhận được công việc",
              "color": "#00ACD4",
              "priority": "high",
              "id": "task_assigned",
              "idDetect": ctx.instance.id || 0,
              "show_in_foreground": "true"
          }
       }
      })
    } else {

    }
  });
};
