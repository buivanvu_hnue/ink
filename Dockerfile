FROM node:10.11.0

WORKDIR /usr/src/app
COPY ./package.json ./package.json
RUN npm install
COPY . .
ENTRYPOINT ["./docker-entrypoint.sh"]
