'use strict';
let app = require('../../server/server')

module.exports = function(Token) {
  Token.observe('before save', async function(ctx) {
    if (ctx.instance && ctx.isNewInstance && ctx.instance.token_key) {
      await Token.destroyAll({
        token_key: ctx.instance.token_key
      })
    }
     if (!ctx.isNewInstance) {
      throw new Error('METHOD_NOT_ALLOWED')
    }
    return true
  })

};
