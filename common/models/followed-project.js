'use strict';
let app = require('../../server/server')
let moment = require('moment')
const getTodayFilter = () => {
  let min = new Date()
  min.setHours(0);
  min.setMinutes(0);
  min.setSeconds(0);
  min.setMilliseconds(0);
  let max = new Date ()
  return {gte: min, lte: max}
}

module.exports = function(FollowedProject) {
  FollowedProject.observe('before save', async function(ctx) {
    let {user, Notification, task} = app.models
    if (ctx.instance) {
      let newData = ctx.instance
      if (!newData.id) {
        return true;
      }
      let currentProject = await FollowedProject.findById(newData.id)
      if (!currentProject) {
        return true
      }
      let usersToSent = []
      let tasks = await task.find({
        where: {
          project_id: currentProject.id
        }
      })
      for (let _task of tasks) {
        if (usersToSent.indexOf(_task.user_id) < 0) {
          usersToSent.push(_task.user_id)
        }
        if (usersToSent.indexOf(_task.created_by) < 0) {
          usersToSent.push(_task.created_by)
        }
      }

      if (currentProject.status_code != newData.status_code) {
        newData.last_status_updated = new Date()
        console.log("SEND NOTIFY WHEN STATUS CHANGED")
        console.log(usersToSent)
        let body = `Dự án ${newData.name} chuyển trạng thái sang ${newData.status}`
        for (let userId of usersToSent) {
          await Notification.sendNotifyTo(userId, {
            "from": newData.updated_by || 0,
            "data": {
             "body": body,
             "title": "Cập nhật trạng thái dự án (" + currentProject.id +")",
             "notification": {
                  "body": body,
                  "title": "Cập nhật trạng thái dự án (" + currentProject.id +")",
                  "color": "#00ACD4",
                  "priority": "high",
                  "id": "project_status_changed",
                  "show_in_foreground": "true",
                  "idDetect": currentProject.id
              }
            }
          })
        }
      }

    } else {
      return true
    }
  });

  FollowedProject.costStat = async function (teamId) {
    teamId = (""+teamId).replace("'", "")
    let db = FollowedProject.dataSource;
    let sql = `SELECT SUM(cost) AS cost_total, status_code FROM FollowedProject WHERE  team_id = ${teamId} GROUP BY status_code`
    let promise = new Promise( (resolve, reject) => {
      db.connector.query(sql, function (err, data) {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
    let data = await promise
    return data
  }

  FollowedProject.countStat = async function (teamId) {
    teamId =  (""+teamId).replace("'", "")
    let db = FollowedProject.dataSource;
    let sql = `SELECT COUNT(*) AS project_count, status_code FROM FollowedProject WHERE  team_id = ${teamId} GROUP BY status_code`
    let promise = new Promise( (resolve, reject) => {
      db.connector.query(sql, function (err, data) {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
    let data = await promise
    return data
  }

  FollowedProject.remoteMethod (
    'costStat',
    {
      http: {path: '/cost-stat/:teamId', verb: 'get'},
      accepts: [{arg: 'teamId', type: 'number', required: true }],
      returns: {arg: 'stat', type: 'object'}
    }
  );

  FollowedProject.remoteMethod (
    'countStat',
    {
      http: {path: '/count-stat/:teamId', verb: 'get'},
      accepts: [{arg: 'teamId', type: 'number', required: true }],
      returns: {arg: 'stat', type: 'object'}
    }
  );
};
