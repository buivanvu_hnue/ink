'use strict';
let app = require('../../server/server')
let moment = require('moment')
const getTodayFilter = () => {
  let min = new Date()
  min.setHours(0);
  min.setMinutes(0);
  min.setSeconds(0);
  min.setMilliseconds(0);
  let max = new Date ()
  return {gte: min, lte: max}
}

const filterByDate = (date) => {
  let min = moment(date).toDate()
  min.setHours(0);
  min.setMinutes(0);
  min.setSeconds(0);
  min.setMilliseconds(0);
  let max = moment(date).endOf('day').toDate()
  return {gte: min, lte: max}
}

module.exports = function(User) {  
  User.withRelated = async function (userId) {
    try { 
      let {FollowedProject, order, token} = app.models
      let user = await User.findById(userId, {include: 'companies'});
      let followedProjects = await FollowedProject.find({where: 
        { is_follow: true, or: [{user_id: user.id}, {team_id: user.team_id ? user.team_id : '-111'}] },
        limit: 200
      })
      let tokens = await token.find({
        where: {user_id: user.id}
      })
      let userOrder = await order.findOne({where: {
        or: [{user_id: user.id}, {team_id: user.team_id ? user.team_id : '-111'}]
      }})
      user.access_tokens = tokens || []
      user.order = userOrder || {}
      user.projects = followedProjects
      return user
    } catch (err) {
      throw err;
    }
  }

  User.getTodayKpi = async function (userId, date) {
    try {
      let {kpiScore} = app.models
      let _userId = userId
      let score = await kpiScore.findOne({
        where: { user_id: _userId, created: date ? filterByDate(date) : getTodayFilter() }
      })
      if (score) {
        return score
      } else {
        return {}
      }

    } catch (err) {
      throw err;
    }
  }

  User.followProject = async function (userId, projectId, options) {
    let {project, FollowedProject} = app.models
    let _project = await project.findById(projectId)
    let user = await User.findById(userId);
    if (!user) {
      throw new Error('USER_ID_INVALID')
    }
    if (!_project) {
      throw new Error('PROJECT_NOT_FOUND');
    }
    let followed = await FollowedProject.findOne({where: 
      { or: [{user_id: user.id}, {team_id: user.team_id ? user.team_id : '-11'}] },
      parent_project_id: _project.id
    })

    if (followed && followed.id) {
      return { project: followed, status: 'already_followed' }
    }
    let clonedProject = _project.toObject();
    delete clonedProject.id;
    clonedProject.user_id = user.id
    clonedProject.team_id = user.team_id || '-1'
    clonedProject.parent_project_id = _project.id
    let newProject = new FollowedProject(clonedProject)
    await newProject.save()
    return { project: clonedProject, status: 'success' }
  }

  User.saveKpi = async function (userId, data, options) {
    let _userId = userId || options.accessToken.userId
    let {kpiScore} = app.models
    if (!data.score) {
      throw new Error('DATA_REQUIRED')
    }
    let score = await kpiScore.findOne({
      where: { user_id: _userId, created: data.created ? filterByDate(data.created) : getTodayFilter() }
    })
    if (score) {
      score.cuoc_goi = data.score.cuoc_goi;
      score.lich_hen_gap = data.score.lich_hen_gap;
      score.chao_gia = data.score.chao_gia;
      score.chot_don_hang = data.score.chot_don_hang;
      score.custom = data.score.custom;
      await score.save()
      score.is_new = false
    } else {
      data.score.user_id = _userId
      data.score.created = new Date()
      score = new kpiScore(data.score)
      await score.save()
      score.is_new = true;
    }
    return (score)
   }

  User.remoteMethod (
    'withRelated',
    {
      http: {path: '/with-related/:userId', verb: 'get'},
      accepts: [{arg: 'id', type: 'number', required: true }],
      returns: {arg: 'user', type: 'object'}
    }
  );

  User.remoteMethod (
    'getTodayKpi',
    {
      http: {path: '/get-today-kpi/:userId', verb: 'get'},
      accepts: [{arg: 'userId', type: 'number', required: true }, {arg: 'date', type: 'date'}],
      returns: {arg: 'kpi', type: 'object'}
    }
  );

  User.remoteMethod (
    'followProject',
    {
      http: {path: '/follow-project/:userId', verb: 'put'},
      accepts: [{arg: 'id', type: 'number', required: true },
        {arg: 'projectId', type: 'number', required: true, http: {source: 'query'}}, 
        {"arg": "options", "type": "object", "http": "optionsFromRequest"}],
      returns: {arg: 'data', root: true, type: 'object'}
    }
  );

  User.remoteMethod (
    'saveKpi',
    {
      http: {path: '/save-kpi/:userId', verb: 'post'},
      accepts: [{arg: 'userId', type: 'number'}, { arg: 'data', type: 'object', http: { source: 'body' } }, {"arg": "options", "type": "object", "http": "optionsFromRequest"}],
      returns: {arg: 'data', root: true, type: 'object'}
    }
  );

};
