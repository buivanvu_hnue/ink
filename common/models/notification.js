'use strict';
const fetch = require('node-fetch')
const KEY = 'AAAAHDeFMH4:APA91bE4R5pOQPFfuDvbi8ha2GrLcSsFwuLcl_oGUUt8FTiCyLz-F4ahFtksDloCSn59JwTR_E9gJHunqaNfJ36rCuWsNFRyqjcslOdG61sOFOuxAUC9dNaVwq7zjDJy5mPj7MaJ6IVL'

let app = require('../../server/server')
let moment = require('moment')
const getTodayFilter = () => {
  let min = new Date()
  min.setHours(0);
  min.setMinutes(0);
  min.setSeconds(0);
  min.setMilliseconds(0);
  let max = new Date ()
  return {gte: min, lte: max}
}


module.exports = function(Notification) {
  Notification.sendNotifyTo = async function (toUserId, data, options) {
      let {user, token} = app.models
      let userObject = await user.findById(toUserId)
      if (!userObject) {
        return console.log("USER_NOT_FOUND")
      }
      const from = data.from || '0'
      const notifyData = data.data
      if (!notifyData || !notifyData.body || !notifyData.title || !notifyData.notification) {
        throw new Error("NO_NOTIFICATION_DATA")
      }
 
      let tokens = await token.find({
        where: {user_id: userObject.id}
      }) || []
      let fcmTokens = tokens.map(t => t.token_key)
      console.log(fcmTokens.length)
      console.log("I will send :", notifyData)
      if (fcmTokens && fcmTokens.length) {
        try {
          let response = await fetch('https://fcm.googleapis.com/fcm/send', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'KEY=' + KEY
            },
            body: JSON.stringify(
              {
                  registration_ids: fcmTokens,
                  notification: {
                    id: notifyData.id || 'view',
                    body: notifyData.body,
                    title: notifyData.title,
                  },
                  data: {
                    notification: notifyData.notification
                  }
            })
          })
          .then(res => res.text())
          .then(body => console.log(body))
        }
        catch (err) {
          console.log(err)
        }
      }
      let notifyToSave = new Notification({
        from,
        to: toUserId,
        data: notifyData,
        created: new Date(),
        sent_count: (fcmTokens && fcmTokens.length) || '0'
      })

      await notifyToSave.save()
      return notifyToSave
    
  }

  Notification.sendNotifyToTeam = async function (teamId, data, options) {
    let {user} = app.models
    let users = await user.find({where: {team_id: teamId}})
    if (!users.length) { console.log("EMPTY TEAM"); return; }
    for (let user of users) {
      try {
        await Notification.sendNotifyTo(user.id, data, options)
      } catch (err) {
        console.log(err)
      }
    }
  }

  Notification.remoteMethod (
    'sendNotifyTo',
    {
      http: {path: '/send-notify-to/:toUserId', verb: 'post'},
      accepts: [{arg: 'toUserId', type: 'number'}, { arg: 'data', type: 'object', http: { source: 'body' } }, {"arg": "options", "type": "object", "http": "optionsFromRequest"}],
      returns: {arg: 'data', root: true, type: 'object'}
    }
  );
};
