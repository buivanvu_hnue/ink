

let fs = require('fs')
let app = require('../server')
const ZONE = {"N":["Lào Cai","Yên Bái","Điện Biên","Hoà Bình","Lai Châu","Sơn La","Hà Giang","Cao Bằng","Bắc Kạn","Lạng Sơn","Tuyên Quang","Thái Nguyên","Phú Thọ","Bắc Giang","Quảng Ninh","Bắc Ninh","Hà Nam","Hà Nội","Hải Dương","Hải Phòng","Hưng Yên","Nam Định","Ninh Bình","Thái Bình","Vĩnh Phúc"],"M":["Thanh Hoá","Nghệ An","Hà Tĩnh","Quảng Bình","Quảng Trị","Thừa Thiên-Huế","Đà Nẵng","Quảng Nam","Quảng Ngãi","Bình Định","Phú Yên","Khánh Hoà","Ninh Thuận","Bình Thuận","Kon Tum","Gia Lai","Đắc Lắc","Đắc Nông","Lâm Đồng"],"S":["Bình Phước","Bình Dương","Đồng Nai","Tây Ninh","Bà Rịa-Vũng Tàu","Thành phố Hồ Chí Minh","Long An","Đồng Tháp","Tiền Giang","An Giang","Bến Tre","Vĩnh Long","Trà Vinh","Hậu Giang","Kiên Giang","Sóc Trăng","Bạc Liêu","Cà Mau","Thành phố Cần Thơ"]}
const moment = require('moment')

module.exports.login = (req, res) => {
  res.render('login');
}

module.exports.logout = (req, res) => {
  res.clearCookie('token');
  res.clearCookie('access_token');
  res.redirect('/login');
}


module.exports.dashboard = (req, res) => {
  if (req.query.token) {
    res.cookie('token', req.query.token);
    res.cookie('access_token', req.query.token);
    req.cookies.token = req.query.token;
  }
  if (!req.cookies.token) {
    res.redirect('/login');
  } else {
    res.render('home');
  }
}

module.exports.projectManage = async (req, res) => {
  let limit = parseInt(req.query.limit) || 25
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let Project = app.models.project
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Image',
      key: 'image',
      image: true
    },
    {
      title: 'Tên dự án',
      key: 'name'
    },
    {
      title: 'Chủ đầu tư/Sở hữu',
      key: 'owner'
    },
    {
      title: 'Giá trị dự án',
      key: 'cost'
    },
    {
      title: 'Địa chỉ',
      key: 'address'
    },
    {
      title: 'Thành phố/Tỉnh',
      key: 'city'
    },
    {
      title: 'Quận/Huyện',
      key: 'district'
    },
    {
      title: 'Trạng thái dự án',
      key: 'status'
    },
    {
      title: 'Ngày khởi công',
      key: 'start'
    },
    {
      title: 'Ngày hoàn công',
      key: 'finish'
    }
  ]
  let projects = []
  let where = {}
  let fieldToQuery = req.query.search_field || 'name'
  if (req.query.q) {
    where = {
      [fieldToQuery]: { like: `%${req.query.q}%` }
    }
  }
  if (req.query.zone) {
    where.zone = req.query.zone
  }
  let count = 0, totalPage =1
  let fieldLabel = ({"name": "Tên dự án", "owner": "Chủ đầu tư", "address": "Địa chỉ", "nha_thau_chinh": "Nhà thầu chính", "city": "Thành phố"})[fieldToQuery]
  // console.log(fieldLabel)
  try{
    projects = await Project.find({
      limit,
      skip,
      where,
      order: 'id DESC'
    })
    count = await Project.count({
      where
    })
    totalPage = Math.ceil(count/limit)
  } catch (err) { res.end(err.message); console.log(err) }
  return res.render('project-manager', {projects, headers, totalPage, fieldToQuery, queryString: req.query.q || '', qlabel: fieldLabel || 'Tên dự án', zone: req.query.zone || 'ALL'})
}

module.exports.projectEdit = async (req, res) => {
  let projectId = req.params.projectId
  let Project = app.models.project
  let project = await Project.findById(projectId)
  if (!project) {
    return res.status(404).render('on404');
  }
  if (project.start) {
    project.start_date = moment(project.start).format("DD/MM/YYYY")
  }
  if (project.finish) {
    project.finish_date = moment(project.finish).format("DD/MM/YYYY")
  }
  project.zone_name = ({N: 'Miền Bắc', M: 'Miền Trung', S: 'Miền Nam'})[project.zone]
  res.render("project-edit", {project})
}

module.exports.createProject = async (req, res) => {
  res.render("project-edit", {project: {}})
}

module.exports.userManage = async (req, res) => {
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Image',
      key: 'avatar',
      image: true
    },
    {
      title: 'Tên đầy đủ',
      key: 'full_name'
    },
    {
      title: 'Email',
      key: 'email'
    },
    {
      title: 'Gói dịch vụ',
      key: 'package'
    },
    {
      title: 'Ngày hết hạn',
      key: 'package_expire'
    },
    {
      title: 'SĐT',
      key: 'phone'
    },
    {
      title: 'Công ty',
      key: 'company'
    },
    {
      title: 'Địa chỉ',
      key: 'address'
    },
    {
      title: 'Địa chỉ Văn phòng',
      key: 'address_office'
    },
    {
      title: 'SĐT Văn phòng',
      key: 'tel_office'
    },
    {
      title: 'Chức vụ',
      key: 'position'
    },
  ]

  let limit = parseInt(req.query.limit) || 50
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let user = app.models.user
  let users = []
  let q = req.query.q, totalPage = 0

  try {
    let db = user.dataSource;
    let join = req.query.package ? 'INNER JOIN' : 'LEFT JOIN'
    let where = ''
    let params = []

    if (req.query.package) {
      where = "WHERE order.package= ?"
      params = [req.query.package]
    }
    if (q) {
      q = '%'+q+'%'
      where = "WHERE email LIKE ? OR full_name LIKE ?"
      params = [q, q]
    }
    let sql = `SELECT user.*, order.package, order.expire_date FROM \`user\` ${join} \`order\` ON user.id = \`order\`.user_id ${where} ORDER BY user.id DESC LIMIT ${skip}, ${limit}`
    let sqlCount = `SELECT count(*) as totalUser FROM \`user\` ${join} \`order\` ON user.id = \`order\`.user_id ${where}`
    let promise = new Promise( (resolve, reject) => {
      db.connector.query(sql, params, function (err, data) {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
    let promiseCount = new Promise( (resolve, reject) => {
      db.connector.query(sqlCount, params, function (err, data) {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
    let data = await promise
    let counter = await promiseCount
    let count = (counter[0] || { totalUser: 0 }).totalUser
    console.log(count)
    totalPage = Math.ceil(count/limit)
    for (let item of data) {
      item.package_expire = item.expire_date && moment(item.expire_date).format("DD/MM/YYYY")
      if (item.active) {
        item.package = item.package + ' - Activated'
      }
    }
    res.render("user-manager", {users: data, headers, package: req.query.package || 'all', q:  req.query.q, totalPage: totalPage, count})
  } catch (err) { res.end(err.message); console.log(err)}

}

module.exports.userEdit = async (req, res) => {
  let userId = req.params.userId
  let user = await app.models.user.findById(userId, {include: "saleTeam" })
  if (!user) {
    return res.status(404).render('on404');
  }
  user = user.toObject()
  res.render("user-edit", {user, userTeam: user.saleTeam || {}})
}


module.exports.userDelete = async (req, res) => {
  let userId = req.params.userId
  try {
    let userModel = app.models.user
    let currentUser = await userModel.findById(userId)
    if (!currentUser || currentUser.username == 'admin') {
      res.status(400).end("CAN'T DELETE")
    }
    await userModel.destroyById(userId)
    res.json({"status": "OK"}) 
  }
  catch (err) {
    console.log(err)
    res.status(400).end(err.message)
  }

}


module.exports.userCreate = async (req, res) => {
  res.render("user-edit", {user: {}, userTeam: {} })
}

module.exports.userUpdate = async (req, res) => {

  let userId = req.params.userId, userModel = app.models.user
  let user

  user = await userModel.findById(userId)
  if (!user) {
    res.status(400).json({message: "INVALID USER ID"})
  }
  Object.keys(req.body).forEach(key => {
    if (key != "password") user[key] = req.body[key]
  })
  if (req.body.password && req.body.password.length > 5) {
    await user.updateAttribute('password', userModel.hashPassword(req.body.password))
  }
  try {
    await user.save()
    return res.json(user)
  } catch (err) {
    return res.status(500).json(err)
  }
}

module.exports.orderList = async (req, res) => {
  let limit = parseInt(req.query.limit) || 25
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let Order = app.models.order
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Tên người mua',
      key: 'user_name'
    },
    {
      title: 'SDT',
      key: 'phone'
    },
    {
      title: 'Gói mua',
      key: 'package'
    },
    {
      title: 'SDT',
      key: 'email'
    },
    {
      title: 'Địa chỉ',
      key: 'address'
    },
    {
      title: 'Thành phố/Tỉnh',
      key: 'city'
    }
  ]
  let orders = []
  let where = {}
  let count = 0, totalPage = 0
  try{
    orders = await Order.find({
      limit,
      skip,
      where,
      order: 'id DESC',
      include: "user"
    })
    count = await Order.count({
      where
    })
    totalPage = Math.ceil(count/limit)
  } catch (err) { console.log(err) }
  let data = orders.map(order => {
    order = order.toObject()
    let user = order.user ||{}
    return {
    id: order.id,
    package: order.package,
    user_name: user.full_name,
    address: user.address,
    phone: user.phone,
    email: user.email,
    city: user.city,
  }})
  return res.render('order-manager', {orders: data, headers, totalPage, count})
}


module.exports.saleTeamManage = async (req, res) => {
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Tên Đội',
      key: 'name'
    },
    {
      title: 'Vùng dữ liệu được truy cập',
      key: 'zone_available',
      enum: {
        '0': '-',
        N: 'Miền Bắc',
        S: 'Miền Name',
        M: 'Miền Trung',
        HCM: 'TP Hồ Chí Minh',
        HN: 'TP Hà Nội'
      }
    }
  ]

  let limit = parseInt(req.query.limit) || 50
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let SaleTeam = app.models.SaleTeam
  let saleTeams = [], totalPage = 0
  try {
    saleTeams = await SaleTeam.find({
      limit,
      skip,
      order: 'id DESC'
    })
    let count = await SaleTeam.count({})
    totalPage = Math.ceil(count/limit)
  } catch (err) { console.log(err)}
  res.render("sale-team-manager", {saleTeams, headers, totalPage})
}


module.exports.sendSystemNotification = async (req, res) => {
  let sysnotifyModel = app.models.sysnotify, Notification = app.models.Notification,  token = app.models.token
  let title = req.body.title
  let content = req.body.content
  let sysnotify = new sysnotifyModel({
    title: title,
    content: content
  })
  try{
    await sysnotify.save()
    let sql = 'SELECT user_id FROM `token` GROUP BY user_id LIMIT 1000'
    let db = token.dataSource
    let promise = new Promise( (resolve, reject) => {
      db.connector.query(sql, function (err, data) {
        if (err) {
          reject(err)
        } else {
          resolve(data)
        }
      })
    })
    let data = await promise
    for (let item of data) {
      let notification = {
        "from": 0,
        "data": {
         "body": content,
         "title": title,
         "notification": {
              "body": content,
              "title": title,
              "color": "#00ACD4",
              "priority": "high",
              "id": "system_notify",
              "show_in_foreground": "true",
              "idDetect": 0
          }
       }
      }
      await Notification.sendNotifyTo(item.user_id, notification)
      console.log('system send notify to' + item.user_id)
    }
    res.end('DONE')
  }
  catch (err) {
    console.log(err)
    res.status(400).end(err.message)
  }
  
}

module.exports.notifyManager = async (req, res) => {
  let headers = [
    {
      title: 'ID',
      key: 'id'
    },
    {
      title: 'Tiêu đề',
      key: 'title'
    },
    {
      title: 'Nội dung',
      key: 'content'
    },
    {
      title: 'Created',
      key: 'created'
    }
  ]

  let limit = parseInt(req.query.limit) || 50
  let page = parseInt(req.query.page) || 1, skip = (page-1)*limit
  let sysnotifyModel = app.models.sysnotify
  let notifies = [], totalPage
  try {
    notifies = await sysnotifyModel.find({
      limit,
      skip,
      order: 'id DESC'
    })
    let count = await sysnotifyModel.count({})
    totalPage = Math.ceil(count/limit)
  } catch (err) { 
    res.end(err.message)
    console.log(err)
  }
  res.render("notify-manager", {notifies, headers, totalPage})
}

module.exports.teamEdit = async (req, res) => {
  try {
    let id = req.params.teamId
    let SaleTeam = app.models.SaleTeam
    let team = await SaleTeam.findById(id)
    if (!team) {
      return res.status(404).end('on404');
    }
    team = team.toObject()
    res.render("team-edit", {team})
  }
  catch (err) {
    console.log(err)
    res.end(err.message)
  }

}
