let app = require('../server')

module.exports.registerContacts = async function  (req, res) {
    let {id, contacts, projectName} = req.body;
    if (!contacts || contacts.length == 0) {
      res.end("empty_contacts")
    }
    let contactsModel = app.models.contacts
    try {
      await contactsModel.destroyAll({
      project_id: id
      })
      for (let contact of contacts) {
        // create new Contact
        contact.project_id = id
        contact.project_name = projectName
        await contactsModel.create(contact)
      }
      res.json({
        status: "success",
        contacts: contacts
      })
    } catch (err) {
      res.status(500).end(err.message)
      console.log(err)
    }

}